#include <stdio.h>
#include <stdlib.h>
#include "equipments.h"


struct Gym {
  int id;
  double square;
};

struct IndexRow {
  int id;
  int address;
};

const char INDEX_FILE[] = "gyms.idx";
const char GARBAGE_FILE[] = "gyms.gb";
const char DATA_FILE[] = "gyms.fl";

void read_gym_from_index(FILE *fptr, int *id, int *address) {
  fread(id, sizeof(int), 1, fptr);
  fread(address, sizeof(int), 1, fptr);
}

int get_address() {
  FILE* garbage_fptr = fopen(GARBAGE_FILE, "r+");
  if (garbage_fptr == NULL) {
    garbage_fptr = fopen(GARBAGE_FILE, "w");
    int zero = 0;
    fwrite(&zero, sizeof(int), 1, garbage_fptr);
  }
  fseek(garbage_fptr, 0, SEEK_END);
  if (ftell(garbage_fptr) == 0) {
    int zero = 0;
    fwrite(&zero, sizeof(int), 1, garbage_fptr);
  }
  fseek(garbage_fptr, 0, SEEK_SET);
  int n;
  fread(&n, sizeof(int), 1, garbage_fptr);
  if (n > 0) {
    fseek(garbage_fptr, sizeof(int) + (n - 1) * sizeof(int), SEEK_SET);
    int address;
    fread(&address, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, 0, SEEK_SET);
    int new_n = n - 1;
    fwrite(&new_n, sizeof(int), 1, garbage_fptr);
    fclose(garbage_fptr);
    return address;
  } else {
    FILE *data_fptr = fopen(DATA_FILE, "r");
    fseek(data_fptr, 0, SEEK_END);
    int m = ftell(data_fptr);
    fclose(garbage_fptr);
    fclose(data_fptr);
    return m;
  }
}

int get_gym_address(int index) {
  FILE *index_fptr = fopen(INDEX_FILE, "r");
  fseek(index_fptr, index * sizeof(struct IndexRow) + sizeof(int), SEEK_SET);
  int address;
  fread(&address, sizeof(int), 1, index_fptr);
  fclose(index_fptr);
  return address;
}

int write_gym_to_data(double *square) {
  int address = get_address();
  FILE *fptr = fopen(DATA_FILE, "r+");
  fseek(fptr, address, SEEK_SET);
  fwrite(square, sizeof(double), 1, fptr);
  int first_equipment = -1;
  fwrite(&first_equipment, sizeof(int), 1, fptr);
  fclose(fptr);
  return address;
}

void write_gym_to_index(FILE *fptr, int *id, int *address) {
  fwrite(id, sizeof(int), 1, fptr);
  fwrite(address, sizeof(int), 1, fptr);
}

// returns all rows that are in index file
struct IndexRow* read_all_index(int * n) {
  FILE* index_fptr = fopen(INDEX_FILE, "r");
  fseek(index_fptr, 0, SEEK_END);
  *n = ftell(index_fptr) / sizeof(struct IndexRow);
  struct IndexRow* rows = (struct IndexRow *) malloc (*n);


  if (*n == 0) {
    return rows;
  }

  fseek(index_fptr, 0, SEEK_SET);

  int i = 0;
  while (!feof(index_fptr)) {
    read_gym_from_index(index_fptr, &rows[i].id, &rows[i].address);
    i++;
  }

  fclose(index_fptr);

  return rows;
}

/**
 * returns the addres of index row of gym
 * if it doesn't exist in index_rows returns -1
 **/
int gym_index(int id) {
  FILE* index_fptr = fopen(INDEX_FILE, "r");

  fseek(index_fptr, 0, SEEK_END);
  int n = ftell(index_fptr);

  int l = 0, r = n / 8 - 1;
  while (l <= r) {
    int m = (l + r) / 2;
    fseek(index_fptr, m * 8, SEEK_SET);
    int key, address;
    read_gym_from_index(index_fptr, &key, &address);

    if (key == id) {
      fclose(index_fptr);
      return m;
    } else if (key > id) {
      r = m - 1;
    } else {
      l = m + 1;
    }
  }
  fclose(index_fptr);
  return -1;
}

/**
 * inserts a gym in database
 * returns 0 if insertion has completed
 * return -1 if an error has occured
 **/
int insert_gym(struct Gym gym) {

  FILE *index_file_ptr;

  int n;
  struct IndexRow *index_rows = read_all_index(&n);


  int index = gym_index(gym.id);

  if (index == -1) {
    if (n == 0) {
      index_file_ptr = fopen(INDEX_FILE, "a");
      int address = 0;
      write_gym_to_index(index_file_ptr, &gym.id, &address);
      fclose(index_file_ptr);

      write_gym_to_data(&gym.square);
      return 0;
    } else {
      int address = write_gym_to_data(&gym.square);

      int inserted = 0;
      index_file_ptr = fopen(INDEX_FILE, "w");
      for (int i = 0; i < n + 1; i++) {
        if (i < n && index_rows[i].id < gym.id) {
          write_gym_to_index(index_file_ptr, &index_rows[i].id, &index_rows[i].address);
        } else if (inserted == 0) {
          inserted = 1;
          write_gym_to_index(index_file_ptr, &gym.id, &address);
        } else {
          write_gym_to_index(index_file_ptr, &index_rows[i - 1].id, &index_rows[i - 1].address);
        }
      }
      fclose(index_file_ptr);
      return 0;
    }
  } else {
    return -1;
  }
}

struct Gym *get_gym(int id) {
  int index = gym_index(id);
  if (index != -1) {
    int address = get_gym_address(index);

    FILE *data_fptr = fopen(DATA_FILE, "r");
    struct Gym* gym = (struct Gym *) malloc (sizeof(struct Gym));
    gym->id = id;
    fseek(data_fptr, address, SEEK_SET);
    fread(&gym->square, sizeof(double), 1, data_fptr);
    int first_equipment;
    fread(&first_equipment, sizeof(int), 1, data_fptr);
    fclose(data_fptr);
    return gym;
  } else {
    return NULL;
  }
}

int get_first_equipment(int id) {
  int index = gym_index(id);
  if (index != -1) {
    int address = get_gym_address(index);

    FILE *data_fptr = fopen(DATA_FILE, "r");
    int first_equipment;
    fseek(data_fptr, address + sizeof(double), SEEK_SET);
    fread(&first_equipment, sizeof(int), 1, data_fptr);
    fclose(data_fptr);
    return first_equipment;
  } else {
    return -2;
  }
}

int gym_is_deleted(int id) {
  int index = gym_index(id);
  return index == -1;
}

/**
 * deletes gym from index file
 * returns 0 if deletion has completed
 * returns -1 if nothing to delete
 **/
int delete_gym(int id) {
  int index = gym_index(id);
  if (index != -1) {
    delete_all(get_first_equipment(id));
    int n;
    struct IndexRow *rows = read_all_index(&n);
    int address = rows[index].address;
    // inserting new block to garbage file
    FILE *garbage_fptr = fopen(GARBAGE_FILE, "r+");
    if (garbage_fptr == NULL) {
      garbage_fptr = fopen(GARBAGE_FILE, "w");
      int zero = 0;
      fwrite(&zero, sizeof(int), 1, garbage_fptr);
    }
    fseek(garbage_fptr, 0, SEEK_END);
    if (ftell(garbage_fptr) == 0) {
      int zero = 0;
      fwrite(&zero, sizeof(int), 1, garbage_fptr);
    }
    fseek(garbage_fptr, 0, SEEK_SET);

    int m;
    fread(&m, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, 0, SEEK_SET);
    int new_m = m + 1;
    fwrite(&new_m, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, m * sizeof(int) + sizeof(int), SEEK_SET);
    fwrite(&address, sizeof(int), 1, garbage_fptr);
    fclose(garbage_fptr);

    FILE *index_fptr = fopen(INDEX_FILE, "w+");
    for (int i = 0; i < n; i++) {
      if (rows[i].id != id) {
        write_gym_to_index(index_fptr, &rows[i].id, &rows[i].address);
      }
    }
    fclose(index_fptr);
    return 0;
  } else {
    return -1;
  }
}

int update_gym(int id, double square) {
  int index = gym_index(id);

  if (index == -1) {
    return -1;
  }

  FILE *data_fptr = fopen(DATA_FILE, "r+");
  int address = get_gym_address(index);
  fseek(data_fptr, address, SEEK_SET);

  if (square >= 0) {
    fwrite(&square, sizeof(double), 1, data_fptr);
  }

  fclose(data_fptr);
  return 0;
}

int set_first_equipment(int gym_id, int equipment_address) {
  int index = gym_index(gym_id);
  int address = get_gym_address(index);
  FILE *gym_data_fptr = fopen(DATA_FILE, "r+");
  fseek(gym_data_fptr, address + sizeof(double), SEEK_SET);
  fwrite(&equipment_address, sizeof(int), 1, gym_data_fptr);
  fclose(gym_data_fptr);
  return 0;
}

int *all_gym_id(int *n) {
  int m;
  struct IndexRow *rows = read_all_index(&m);
  int *res = malloc(sizeof(int) * m);
  *n = m;
  for (int i = 0; i < m; i++) {
    res[i] = rows[i].id;
  }
  return res;
}
