#ifndef HEADER_FILE
#define HEADER_FILE
struct Gym {
  int id;
  double square;
};
int insert_gym(struct Gym gym);
struct Gym *get_gym(int id);
int gym_is_deleted(int id);
int delete_gym(int id);
int update_gym(int id, double square);
int gym_index(int id);
int get_first_equipment(int id);
int get_gym_address(int id);
int set_first_equipment(int room_idx, int address);
int *all_gym_id(int *n);
#endif
