#include <stdio.h>
#include <string.h>

const char *INDEX_EQUIPMENTS_FILE = "equipments.idx";
const char *DATA_EQUIPMENTS_FILE = "equipments.fl";
const char *GARBAGE_EQUIPMENTS_FILE = "equipments.gb";
const char INDEX_FILE[] = "gyms.idx";
const char GARBAGE_FILE[] = "gyms.gb";
const char DATA_FILE[] = "gyms.fl";

const char *GET_GYM = "get_gym";
const char *INSERT_GYM = "insert_gym";
const char *GYM_IS_DELETED = "gym_is_deleted";
const char *DELETE_GYM = "delete_gym";
const char *UPDATE_GYM = "update_gym";
const char *INSERT_EQUIPMENT = "insert_equipment";
const char *GET_EQUIPMENT = "get_equipment";
const char *DELETE_EQUIPMENT = "delete_equipment";
const char *UPDATE_EQUIPMENT = "update_equipment";
const char *ALL_GYM_ID = "all_gym_count";
const int NAME_LENGTH = 20;


struct Equipment {
  int id;
  int difficulty;
  int gym_id;
};

struct Index_row {
  int id;
  int address;
};

void read_equipment_from_index(FILE* fptr, int *key, int *address) {
  fread(key, sizeof(int), 1, fptr);
  fread(address, sizeof(int), 1, fptr);
}

int get_equipment_address(int index) {
  FILE *index_fptr = fopen(INDEX_EQUIPMENTS_FILE, "r");
  fseek(index_fptr, index * sizeof(struct Index_row) + sizeof(int), SEEK_SET);
  int address;
  fread(&address, sizeof(int), 1, index_fptr);
  fclose(index_fptr);
  return address;
}

int get_address_for_equipment() {
  FILE *garbage_fptr = fopen(GARBAGE_EQUIPMENTS_FILE, "r+");
  if (garbage_fptr == NULL) {
    garbage_fptr = fopen(GARBAGE_EQUIPMENTS_FILE, "w");
    int zero = 0;
    fwrite(&zero, sizeof(int), 1, garbage_fptr);
  }
  fseek(garbage_fptr, 0, SEEK_END);
  int n = ftell(garbage_fptr);
  fseek(garbage_fptr, 0, SEEK_SET);
  if (n == 0) {
    int zero = 0;
    fwrite(&zero, sizeof(int), 1, garbage_fptr);
  }
  fseek(garbage_fptr, 0, SEEK_SET);

  int address;
  int m;
  fread(&m, sizeof(int), 1, garbage_fptr);
  if (m == 0) {
    FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r");
    fseek(data_fptr, 0, SEEK_END);
    address = ftell(data_fptr);
    fclose(data_fptr);
  } else {
    fseek(garbage_fptr, m * sizeof(int), SEEK_SET);
    fread(&address, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, 0, SEEK_SET);
    int new_m = m - 1;
    fwrite(&new_m, sizeof(int), 1, garbage_fptr);
  }

  fclose(garbage_fptr);
  return address;
}

int equipment_index(int id) {
  FILE* index_fptr = fopen(INDEX_EQUIPMENTS_FILE, "r");

  fseek(index_fptr, 0, SEEK_END);
  int n = ftell(index_fptr);

  int l = 0, r = n / 8 - 1;
  while (l <= r) {
    int m = (l + r) / 2;
    fseek(index_fptr, m * sizeof(struct Index_row), SEEK_SET);
    int key, address;
    read_equipment_from_index(index_fptr, &key, &address);

    if (key == id) {
      fclose(index_fptr);
      return m;
    } else if (key > id) {
      r = m - 1;
    } else {
      l = m + 1;
    }
  }
  fclose(index_fptr);
  return -1;
}

struct Index_row *get_all_index(int *n) {
  FILE *index_fptr = fopen(INDEX_EQUIPMENTS_FILE, "r");
  fseek(index_fptr, 0, SEEK_END);
  *n = ftell(index_fptr) / 8;
  fseek(index_fptr, 0, SEEK_SET);
  struct Index_row *rows = (struct Index_row*) malloc (sizeof(struct Index_row));
  if (n == 0) {
    fclose(index_fptr);
    return rows;
  }

  int i = 0;
  while (!feof(index_fptr)) {
    int key, address;
    fread(&key, sizeof(int), 1, index_fptr);
    fread(&address, sizeof(int), 1, index_fptr);
    rows[i].id = key;
    rows[i].address = address;
    i++;
  }

  fclose(index_fptr);
  return rows;
}

int insert_equipment_to_index(int id, int address) {
  int n = 0;
  struct Index_row *rows = get_all_index(&n);

  if (n == 0) {
    FILE *index_fptr = fopen(INDEX_EQUIPMENTS_FILE, "w");
    fwrite(&id, sizeof(int), 1, index_fptr);
    fwrite(&address, sizeof(int), 1, index_fptr);
    fclose(index_fptr);
  } else {
    FILE *index_fptr = fopen(INDEX_EQUIPMENTS_FILE, "w");
    int inserted = 0;
    for (int i = 0; i < n + 1; i++) {
      if (i != n && rows[i].id < id) {
        fwrite(&rows[i].id, sizeof(int), 1, index_fptr);
        fwrite(&rows[i].address, sizeof(int), 1, index_fptr);
      } else if (inserted == 0) {
        inserted = 1;
        fwrite(&id, sizeof(int), 1, index_fptr);
        fwrite(&address, sizeof(int), 1, index_fptr);
      } else {
        fwrite(&rows[i - 1].id, sizeof(int), 1, index_fptr);
        fwrite(&rows[i - 1].address, sizeof(int), 1, index_fptr);
      }
    }
    fclose(index_fptr);
  }
  return 0;
}

int insert_equipment(int gym_id, struct Equipment equipment) {
  int equipment_idx = equipment_index(equipment.id);

  if (equipment_idx != -1) {
    return -1;
  } else {
    int gym_idx = gym_index(gym_id);
    if (gym_idx == -1) {
      return -1;
    }

    int first_equipment = get_first_equipment(gym_id);
    int address = get_address_for_equipment();

    FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
    if (first_equipment != -1) {
      fseek(data_fptr, first_equipment + sizeof(int) * 3, SEEK_SET);
      fwrite(&address, sizeof(int), 1, data_fptr);
    }

    insert_equipment_to_index(equipment.id, address);

    fseek(data_fptr, address, SEEK_SET);
    fwrite(&equipment.id, sizeof(int), 1, data_fptr);
    fwrite(&equipment.difficulty, sizeof(int), 1, data_fptr);
    fwrite(&gym_id, sizeof(int), 1, data_fptr);
    int minus_one = -1;
    fwrite(&minus_one, sizeof(int), 1, data_fptr);
    fwrite(&first_equipment, sizeof(int), 1, data_fptr);
    fclose(data_fptr);

    set_first_equipment(gym_id, address);

    return 0;
  }
  return -1;
}

struct Equipment *get_equipment(int id) {
  int index = equipment_index(id);
  if (index == -1)
    return NULL;
  int address = get_equipment_address(index);
  FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r");
  fseek(data_fptr, address, SEEK_SET);
  struct Equipment *equipment = malloc(sizeof(struct Equipment));
  fread(&equipment->id, sizeof(int), 1, data_fptr);
  fread(&equipment->difficulty, sizeof(int), 1, data_fptr);
  fread(&equipment->gym_id, sizeof(int), 1, data_fptr);
  int prev;
  fread(&prev, sizeof(int), 1, data_fptr);
  int next;
  fread(&next, sizeof(int), 1, data_fptr);
  fclose(data_fptr);
  return equipment;
}

void set_next(int address, int next) {
  FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
  fseek(data_fptr, address + sizeof(int) * 4, SEEK_SET);
  fwrite(&next, sizeof(int), 1, data_fptr);
  fclose(data_fptr);
}

int get_prev(int address) {
  FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
  fseek(data_fptr, address + sizeof(int) * 3, SEEK_SET);
  int prev;
  fread(&prev, sizeof(int), 1, data_fptr);
  fclose(data_fptr);
  return prev;
}

void set_prev(int address, int prev) {
  FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
  fseek(data_fptr, address + sizeof(int) * 3, SEEK_SET);
  fwrite(&prev, sizeof(int), 1, data_fptr);
  fclose(data_fptr);
}

int get_next(int address) {
  FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
  fseek(data_fptr, address + sizeof(int) * 4, SEEK_SET);
  int next;
  fread(&next, sizeof(int), 1, data_fptr);
  fclose(data_fptr);
  return next;
}

int delete_equipment(int id) {
  int index = equipment_index(id);
  if (index == -1) {
    return -1;
  } else {
    FILE *data_fptr;
    int address = get_equipment_address(index);
    int prev = get_prev(address);

    if (prev == -1) {
      data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
      fseek(data_fptr, address + 2 * sizeof(int), SEEK_SET);
      int gym_id;
      fread(&gym_id, sizeof(int), 1, data_fptr);
      fclose(data_fptr);
      int next = get_next(address);
      set_prev(next, -1);
      set_first_equipment(gym_id, next);
    } else {
      int next = get_next(address);
      set_next(prev, next);
      if (next != -1)
        set_prev(next, prev);
    }

    FILE *garbage_fptr = fopen(GARBAGE_EQUIPMENTS_FILE, "r+");
    int n;
    fread(&n, sizeof(int), 1, garbage_fptr);
    int new_n = n + 1;
    fseek(garbage_fptr, 0, SEEK_SET);
    fwrite(&new_n, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, 0 , SEEK_END);
    fwrite(&address, sizeof(int), 1, garbage_fptr);
    fclose(garbage_fptr);

    int m;
    struct Index_row *rows = get_all_index(&m);
    FILE *index_fptr = fopen(INDEX_EQUIPMENTS_FILE, "w");
    for (int i = 0; i < m; i++) {
      if (rows[i].id != id) {
        fwrite(&rows[i].id, sizeof(int), 1, index_fptr);
        fwrite(&rows[i].address, sizeof(int), 1, index_fptr);
      }
    }
    fclose(index_fptr);

    return 0;
  }
}

int update_equipment(int id, struct Equipment equipment) {
  int index = equipment_index(id);
  if (index == -1) {
    return -1;
  }
  int gym_idx = gym_index(equipment.gym_id);
  if (gym_idx == -1) {
    return -1;
  }
  delete_equipment(id);
  insert_equipment(equipment.gym_id, equipment);
  return 0;
}

void delete_all(int address) {
  while (address != -1) {
    int temp = get_next(address);
    int id;
    FILE *data_fptr = fopen(DATA_EQUIPMENTS_FILE, "r+");
    fseek(data_fptr, address, SEEK_SET);
    fread(&id, sizeof(int), 1, data_fptr);
    fclose(data_fptr);
    delete_equipment(id);
    address = temp;
  }
}

int *all_equipments_id(int *n) {
  struct Index_row *rows = get_all_index(n);
  int *res = malloc(sizeof(int) * (*n));
  for (int i = 0; i < *n; i++) {
    res[i] = rows[i].id;
  }
  return res;
}

struct Gym {
  int id;
  double square;
};

struct IndexRow {
  int id;
  int address;
};

//

void read_gym_from_index(FILE *fptr, int *id, int *address) {
  fread(id, sizeof(int), 1, fptr);
  fread(address, sizeof(int), 1, fptr);
}

int get_address() {
  FILE* garbage_fptr = fopen(GARBAGE_FILE, "r+");
  if (garbage_fptr == NULL) {
    garbage_fptr = fopen(GARBAGE_FILE, "w");
    int zero = 0;
    fwrite(&zero, sizeof(int), 1, garbage_fptr);
  }
  fseek(garbage_fptr, 0, SEEK_END);
  if (ftell(garbage_fptr) == 0) {
    int zero = 0;
    fwrite(&zero, sizeof(int), 1, garbage_fptr);
  }
  fseek(garbage_fptr, 0, SEEK_SET);
  int n;
  fread(&n, sizeof(int), 1, garbage_fptr);
  if (n > 0) {
    fseek(garbage_fptr, sizeof(int) + (n - 1) * sizeof(int), SEEK_SET);
    int address;
    fread(&address, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, 0, SEEK_SET);
    int new_n = n - 1;
    fwrite(&new_n, sizeof(int), 1, garbage_fptr);
    fclose(garbage_fptr);
    return address;
  } else {
    FILE *data_fptr = fopen(DATA_FILE, "r");
    fseek(data_fptr, 0, SEEK_END);
    int m = ftell(data_fptr);
    fclose(garbage_fptr);
    fclose(data_fptr);
    return m;
  }
}

int get_gym_address(int index) {
  FILE *index_fptr = fopen(INDEX_FILE, "r");
  fseek(index_fptr, index * sizeof(struct IndexRow) + sizeof(int), SEEK_SET);
  int address;
  fread(&address, sizeof(int), 1, index_fptr);
  fclose(index_fptr);
  return address;
}

int write_gym_to_data(double *square) {
  int address = get_address();
  FILE *fptr = fopen(DATA_FILE, "r+");
  fseek(fptr, address, SEEK_SET);
  fwrite(square, sizeof(double), 1, fptr);
  int first_equipment = -1;
  fwrite(&first_equipment, sizeof(int), 1, fptr);
  fclose(fptr);
  return address;
}

void write_gym_to_index(FILE *fptr, int *id, int *address) {
  fwrite(id, sizeof(int), 1, fptr);
  fwrite(address, sizeof(int), 1, fptr);
}

// returns all rows that are in index file
struct IndexRow* read_all_index(int * n) {
  FILE* index_fptr = fopen(INDEX_FILE, "r");
  fseek(index_fptr, 0, SEEK_END);
  *n = ftell(index_fptr) / sizeof(struct IndexRow);
  struct IndexRow* rows = (struct IndexRow *) malloc (*n);


  if (*n == 0) {
    return rows;
  }

  fseek(index_fptr, 0, SEEK_SET);

  int i = 0;
  while (!feof(index_fptr)) {
    read_gym_from_index(index_fptr, &rows[i].id, &rows[i].address);
    i++;
  }

  fclose(index_fptr);

  return rows;
}

/**
 * returns the addres of index row of gym
 * if it doesn't exist in index_rows returns -1
 **/
int gym_index(int id) {
  FILE* index_fptr = fopen(INDEX_FILE, "r");

  fseek(index_fptr, 0, SEEK_END);
  int n = ftell(index_fptr);

  int l = 0, r = n / 8 - 1;
  while (l <= r) {
    int m = (l + r) / 2;
    fseek(index_fptr, m * 8, SEEK_SET);
    int key, address;
    read_gym_from_index(index_fptr, &key, &address);

    if (key == id) {
      fclose(index_fptr);
      return m;
    } else if (key > id) {
      r = m - 1;
    } else {
      l = m + 1;
    }
  }
  fclose(index_fptr);
  return -1;
}

/**
 * inserts a gym in database
 * returns 0 if insertion has completed
 * return -1 if an error has occured
 **/
int insert_gym(struct Gym gym) {

  FILE *index_file_ptr;

  int n;
  struct IndexRow *index_rows = read_all_index(&n);


  int index = gym_index(gym.id);

  if (index == -1) {
    if (n == 0) {
      index_file_ptr = fopen(INDEX_FILE, "a");
      int address = 0;
      write_gym_to_index(index_file_ptr, &gym.id, &address);
      fclose(index_file_ptr);

      write_gym_to_data(&gym.square);
      return 0;
    } else {
      int address = write_gym_to_data(&gym.square);

      int inserted = 0;
      index_file_ptr = fopen(INDEX_FILE, "w");
      for (int i = 0; i < n + 1; i++) {
        if (i < n && index_rows[i].id < gym.id) {
          write_gym_to_index(index_file_ptr, &index_rows[i].id, &index_rows[i].address);
        } else if (inserted == 0) {
          inserted = 1;
          write_gym_to_index(index_file_ptr, &gym.id, &address);
        } else {
          write_gym_to_index(index_file_ptr, &index_rows[i - 1].id, &index_rows[i - 1].address);
        }
      }
      fclose(index_file_ptr);
      return 0;
    }
  } else {
    return -1;
  }
}

struct Gym *get_gym(int id) {
  int index = gym_index(id);
  if (index != -1) {
    int address = get_gym_address(index);

    FILE *data_fptr = fopen(DATA_FILE, "r");
    struct Gym* gym = (struct Gym *) malloc (sizeof(struct Gym));
    gym->id = id;
    fseek(data_fptr, address, SEEK_SET);
    fread(&gym->square, sizeof(double), 1, data_fptr);
    int first_equipment;
    fread(&first_equipment, sizeof(int), 1, data_fptr);
    fclose(data_fptr);
    return gym;
  } else {
    return NULL;
  }
}

int get_first_equipment(int id) {
  int index = gym_index(id);
  if (index != -1) {
    int address = get_gym_address(index);

    FILE *data_fptr = fopen(DATA_FILE, "r");
    int first_equipment;
    fseek(data_fptr, address + sizeof(double), SEEK_SET);
    fread(&first_equipment, sizeof(int), 1, data_fptr);
    fclose(data_fptr);
    return first_equipment;
  } else {
    return -2;
  }
}

int gym_is_deleted(int id) {
  int index = gym_index(id);
  return index == -1;
}

/**
 * deletes gym from index file
 * returns 0 if deletion has completed
 * returns -1 if nothing to delete
 **/
int delete_gym(int id) {
  int index = gym_index(id);
  if (index != -1) {
    delete_all(get_first_equipment(id));
    int n;
    struct IndexRow *rows = read_all_index(&n);
    int address = rows[index].address;
    // inserting new block to garbage file
    FILE *garbage_fptr = fopen(GARBAGE_FILE, "r+");
    if (garbage_fptr == NULL) {
      garbage_fptr = fopen(GARBAGE_FILE, "w");
      int zero = 0;
      fwrite(&zero, sizeof(int), 1, garbage_fptr);
    }
    fseek(garbage_fptr, 0, SEEK_END);
    if (ftell(garbage_fptr) == 0) {
      int zero = 0;
      fwrite(&zero, sizeof(int), 1, garbage_fptr);
    }
    fseek(garbage_fptr, 0, SEEK_SET);

    int m;
    fread(&m, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, 0, SEEK_SET);
    int new_m = m + 1;
    fwrite(&new_m, sizeof(int), 1, garbage_fptr);
    fseek(garbage_fptr, m * sizeof(int) + sizeof(int), SEEK_SET);
    fwrite(&address, sizeof(int), 1, garbage_fptr);
    fclose(garbage_fptr);

    FILE *index_fptr = fopen(INDEX_FILE, "w+");
    for (int i = 0; i < n; i++) {
      if (rows[i].id != id) {
        write_gym_to_index(index_fptr, &rows[i].id, &rows[i].address);
      }
    }
    fclose(index_fptr);
    return 0;
  } else {
    return -1;
  }
}

int update_gym(int id, double square) {
  int index = gym_index(id);

  if (index == -1) {
    return -1;
  }

  FILE *data_fptr = fopen(DATA_FILE, "r+");
  int address = get_gym_address(index);
  fseek(data_fptr, address, SEEK_SET);

  if (square >= 0) {
    fwrite(&square, sizeof(double), 1, data_fptr);
  }

  fclose(data_fptr);
  return 0;
}

int set_first_equipment(int gym_id, int equipment_address) {
  int index = gym_index(gym_id);
  int address = get_gym_address(index);
  FILE *gym_data_fptr = fopen(DATA_FILE, "r+");
  fseek(gym_data_fptr, address + sizeof(double), SEEK_SET);
  fwrite(&equipment_address, sizeof(int), 1, gym_data_fptr);
  fclose(gym_data_fptr);
  return 0;
}

int *all_gym_id(int *n) {
  int m;
  struct IndexRow *rows = read_all_index(&m);
  int *res = malloc(sizeof(int) * m);
  *n = m;
  for (int i = 0; i < m; i++) {
    res[i] = rows[i].id;
  }
  return res;
}


void process_all_gym_id() {
  int n;
  int *ids = all_gym_id(&n);
  printf("-\n");
  printf("%d\n", n);
  printf("-\n");
}

void process_all_equipment_id() {
  int n;
  int *ids = all_equipments_id(&n);
  printf("-\n");
  for (int i = 0; i < n; i++) {
    printf("%d\n", ids[i]);
  }
  printf("-\n");
}

void process_update_equipment() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  printf("Enter difficulty: ");
  int difficulty;
  scanf("%d", &difficulty);
  printf("Enter gym id: ");
  int gym_id;
  scanf("%d", &gym_id);
  struct Equipment equipment = {.id = id, .difficulty = difficulty, .gym_id = gym_id};
  if (update_equipment(id, equipment) != -1) {
    printf("------------------------\n");
    printf("Updated equipment successfully\n");
    printf("------------------------\n");
  } else {
    printf("----------------------------------------------------------------\n");
    printf("Equipment with such id already exists or there is no gym with such id\n");
    printf("----------------------------------------------------------------\n");

  }
}

void process_delete_equipment() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  if (delete_equipment(id) == 0) {
    printf("----------------------------\n");
    printf("Equipment was deleted successfully\n");
    printf("----------------------------\n");
  } else {
    printf("------------------------------\n");
    printf("Equipment with such id doesn't exist\n");
    printf("------------------------------\n");
  }
}

void process_insert_equipment() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  printf("Enter difficulty ");
  int difficulty;
  scanf("%d", &difficulty);
  printf("Enter gym id: ");
  int gym_id;
  scanf("%d", &gym_id);
  struct Equipment equipment= {.id = id, .difficulty = difficulty, .gym_id = gym_id};
  if (insert_equipment(gym_id, equipment) != -1) {
    printf("----------------------------\n");
    printf("Successfully added a new equipment\n");
    printf("----------------------------\n");
  } else {
    printf("----------------------------------------------------------------\n");
    printf("Equipment with such id already exists or there is no gym with such id\n");
    printf("----------------------------------------------------------------\n");

  }

}

void process_get_equipment() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  struct Equipment *equipment = get_equipment(id);
  if (equipment == NULL) {
    printf("----------------------------------\n");
    printf("The equipment with such id doesn't exist\n");
    printf("----------------------------------\n");
    return;
  }
  printf("-------------------\n");
  printf("id: %d\n", equipment->id);
  printf("difficulty: %d\n", equipment->difficulty);
  printf("gym_id: %d\n", equipment->gym_id);

  printf("-------------------\n");
}

void process_update_gym() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  printf("Enter new square: ");
  double square;
  scanf("%lf", &square);
  if (update_gym(id, square) == -1) {
    printf("-----------------------------------\n");
    printf("The gym with such id doesn't exist\n");
    printf("-----------------------------------\n");
  } else {
    printf("-----------------------------\n");
    printf("Successfully updated the gym\n");
    printf("-----------------------------\n");

  }
}

void process_delete_gym() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  delete_gym(id);
  printf("Successfully deleted gym\n");
}

void process_gym_is_deleted() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  if (gym_is_deleted(id)) {
    printf("YES\n");
  } else {
    printf("NO\n");
  }
}

void process_get_gym() {
  printf("Enter id: ");
  int id;
  scanf("%d", &id);
  struct Gym *gym = get_gym(id);

  if (gym != NULL) {
    printf("---------------------\n");
    printf("Gym:\n");
    printf("id: %d\n", gym->id);
    printf("square: %lf\n", gym->square);
    printf("---------------------\n");
  } else {
    printf("---------------------------------\n");
    printf("The gym with id %d doesn't exist\n", id);
    printf("---------------------------------\n");
  }
}

void process_insert_gym() {
  int id;
  printf("id: ");
  scanf("%d", &id);
  printf("square : ");
  double square;
  scanf("%lf", &square);
  struct Gym gym= {.id = id, .square = square};
  if (insert_gym(gym) == 0) {
    printf("--------------------------------\n");
    printf("Successfully added a new gym\n");
    printf("--------------------------------\n");
  } else {
    printf("------------------------------------\n");
    printf("The gym with such id already exists\n");
    printf("------------------------------------\n");
  }
}

int main() {
  while (1) {
    char command[30];
    printf(">>> ");
    scanf("%29s", command);
    if (strcmp(command, GET_GYM) == 0) {
      process_get_gym();
    } else if (strcmp(command, INSERT_GYM) == 0) {
      process_insert_gym();
    } else if (strcmp(command, GYM_IS_DELETED) == 0) {
      process_gym_is_deleted();
    } else if (strcmp(command, DELETE_GYM) == 0) {
      process_delete_gym();
    } else if (strcmp(command, UPDATE_GYM) == 0) {
      process_update_gym();
    } else if (strcmp(command, GET_EQUIPMENT) == 0) {
      process_get_equipment();
    } else if (strcmp(command, INSERT_EQUIPMENT) == 0) {
      process_insert_equipment();
    } else if (strcmp(command, DELETE_EQUIPMENT) == 0) {
      process_delete_equipment();
    } else if (strcmp(command, UPDATE_EQUIPMENT) == 0) {
      process_update_equipment();
    } else if (strcmp(command, ALL_GYM_ID) == 0) {
      process_all_gym_id();
    }
  }
}
