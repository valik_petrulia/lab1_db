struct Equipment {
    int id;
    int difficulty;
    int gym_id;
};
int insert_equipment(int gym_id, struct Equipment equipment);
struct Equipment *get_equipment(int id);
int delete_equipment(int id);
int update_equipment(int id, struct Equipment equipment);
void delete_all(int id);
int *all_equipments_id(int *n);
